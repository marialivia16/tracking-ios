//
//  CustomCell.m
//  Hiking
//
//  Created by Maria Livia on 4/23/15.
//  Copyright (c) 2015 Hiking Inc. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell

@synthesize titleLabel = _titleLabel;
@synthesize userPicture = _userPicture;
@synthesize kmLabel = _kmLabel;
@synthesize timeLabel = _timeLabel;
@synthesize dateLabel = _dateLabel;
@synthesize favIcon = _favIcon;
@synthesize descriptionLabel = _descriptionLabel;
@synthesize detailsButton = _detailsButton;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
