//
//  ViewController.h
//  Hiking
//
//  Created by LaboratoriOS Cronian Academy on 05/03/15.
//  Copyright (c) 2015 Hiking Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import <Parse/Parse.h>
#import <FacebookSDK/FacebookSDK.h>
#import "Reachability.h"

@interface MapViewController : UIViewController <CLLocationManagerDelegate>
@property (strong, nonatomic)  GMSMapView *myMapView;
@property (strong, nonatomic) GMSCameraPosition *myCameraPosition;
@property (nonatomic) CLLocationManager *mLocationManager;
@property (nonatomic) NSMutableArray *mLocationArray;
@property (nonatomic) GMSMutablePath *mPath;
@property (nonatomic) GMSPolyline *mPolyline;
@property (nonatomic) UIButton *mStartButton;
@property (nonatomic) UILabel *mInfoLabel;
@property (nonatomic) UIButton *startTrackingBtn;
@property (nonatomic) NSNumber *counter;
@property NSString *mTrackId;
@property NSString *userFacebookId;
@property Reachability *reachability;
@property NSNumber *trackDistance;
@property NSNumber *trackDuration;
@property NSString *trackName,*trackDescription;
@property CLLocation *lastLocation;
@property PFObject *track;
@property NSDate *startDate, *stopDate;
@property UIView *infoView;
@property UILabel *speedLabel, *timeLabel, *distanceLabel;
@property Boolean *isTracking;

@end

