//
//  CustomCell.h
//  Hiking
//
//  Created by Maria Livia on 4/23/15.
//  Copyright (c) 2015 Hiking Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface CustomCell : UITableViewCell
@property (nonatomic, weak) IBOutlet FBProfilePictureView *userPicture;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *kmLabel;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UIButton *favIcon;
@property (nonatomic, weak) IBOutlet UIButton *detailsButton;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;
@end
