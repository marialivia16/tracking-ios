//
//  AppDelegate.m
//  Hiking
//
//  Created by LaboratoriOS Cronian Academy on 05/03/15.
//  Copyright (c) 2015 Hiking Inc. All rights reserved.
//

#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import "UIImage+Helpers.h"
#import <Parse/Parse.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [GMSServices provideAPIKey:@"AIzaSyB704Il9H0NgTloclXkf_MilqlCrlovw5g"];
    
    _window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    [self initTabBarController];
    [self configureNavigationBar];
    
    [_window setRootViewController:_mainTabBarController];
    [_window makeKeyAndVisible];
    
    [Parse enableLocalDatastore];
    
    [Parse setApplicationId:@"Ig8byRIdjPJ10gaoTGbNGlmC7aoc6GJZ8LPDOQm8"
                  clientKey:@"UzjxW7KSPsdBpRyX3S75rF6P95BmcrBSV60XffB8"];
    
    return YES;
}

-(void)initTabBarController{
    _mainTabBarController = [[UITabBarController alloc] init];
    
    _newsFeedViewController = [[NewsFeedViewController alloc] init];
    _newsFeedViewController.title = @"NewsFeed";
    UINavigationController *newsFeedNavigationController = [[UINavigationController alloc] initWithRootViewController:_newsFeedViewController];
    
    _mapViewController = [[MapViewController alloc] init];
    _mapViewController.title = @"Hike";
    UINavigationController *mapNavigationController = [[UINavigationController alloc] initWithRootViewController:_mapViewController];
    
    _profileViewController = [[ProfileViewController alloc] init];
    _profileViewController.title = @"Profile";
    UINavigationController *profileNavigationController = [[UINavigationController alloc] initWithRootViewController:_profileViewController];
    
    [_mainTabBarController setViewControllers:@[newsFeedNavigationController, mapNavigationController, profileNavigationController]];
    
    [[UITabBar appearance] setTranslucent:NO];
    
    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:0.95 green:0.95 blue:0.94 alpha:1.0]];
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:50/255.0f green:200/255.0f blue:180/255.0f alpha:1.0f]];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-DemiBold" size:11.0f], NSForegroundColorAttributeName : [UIColor colorWithRed:0.42 green:0.47 blue:0.53 alpha:1.0]} forState:UIControlStateNormal];
    
    [self configureTabBarItems];
}

-(void)configureTabBarItems{
    UITabBarItem *item0 = [self.mainTabBarController.tabBar.items objectAtIndex:0];
    item0.image = [[[UIImage imageNamed:@"NewsFeedIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] scaledToSize:CGSizeMake(25, 25)];
    item0.selectedImage =[[UIImage imageNamed:@"NewsFeedIcon"] scaledToSize:CGSizeMake(25, 25)];
    
    UITabBarItem *item1 = [self.mainTabBarController.tabBar.items objectAtIndex:1];
    item1.image = [[[UIImage imageNamed:@"HikingIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] scaledToSize:CGSizeMake(25, 25)];
    item1.selectedImage =[[UIImage imageNamed:@"HikingIcon"] scaledToSize:CGSizeMake(25, 25)];
    
    UITabBarItem *item2 = [self.mainTabBarController.tabBar.items objectAtIndex:2];
    item2.image = [[[UIImage imageNamed:@"ProfileIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] scaledToSize:CGSizeMake(25, 25)];
    item2.selectedImage =[[UIImage imageNamed:@"ProfileIcon"] scaledToSize:CGSizeMake(25, 25)];
    
    [self.mainTabBarController setSelectedIndex:1];
}

-(void)configureNavigationBar{
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],NSFontAttributeName: [UIFont fontWithName:@"AvenirNext-DemiBold" size:16.0],}];
    
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:50/255.0f green:200/255.0f blue:180/255.0f alpha:1.0f]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];    
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    //attempt to extract a token from the url
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    //Logs 'install' and 'app active' App Events
    [FBAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
