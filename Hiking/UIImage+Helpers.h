//
//  UIImage+Helpers.h
//  Hiking
//
//  Created by LaboratoriOS Cronian Academy on 26/03/15.
//  Copyright (c) 2015 Hiking Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Helpers)
- (UIImage *)imageWithColor:(UIColor *)color;
- (UIImage *)scaledToSize:(CGSize)newSize;
+ (UIImage *)imageFromColor:(UIColor *)color;
@end
