//
//  TrackDetailViewController.m
//  Hiking
//
//  Created by Maria Livia on 4/28/15.
//  Copyright (c) 2015 Hiking Inc. All rights reserved.
//

#import "TrackDetailViewController.h"

@interface TrackDetailViewController ()

@end

@implementation TrackDetailViewController

- (instancetype) initWithTrack:(NSString *)trackId{
    if(self = [super init]){
        _trackId = trackId;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _myMapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.view.frame.size.width, (self.view.frame.size.height / 2 - 40)) camera:_myCameraPosition];
    _myMapView.mapType = kGMSTypeHybrid;
    
    _mPath = [GMSMutablePath path];
    
    _mPolyline = [GMSPolyline polylineWithPath:_mPath];
    _mPolyline.map = _myMapView;
    _mPolyline.strokeColor = [UIColor blueColor];
    _mPolyline.strokeWidth = 3.f;
    
    [self.view addSubview:_myMapView];
    [self initInfoView];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Track"];
    query.limit = 20;
    [query orderByDescending:@"date"];
    [query whereKey:@"track_id" equalTo:_trackId];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){
            //update track details
            [self getUserName:[[objects firstObject] valueForKey:@"user_id"]];
            _userPicture.profileID = [[objects firstObject] valueForKey:@"user_id"];
            _trackTitle.text = [[objects firstObject] valueForKey:@"name"];
            
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            [formatter setMaximumFractionDigits:2];
            NSNumber *dist = [[objects firstObject] valueForKey:@"distance"];
            _trackDistance.text = [NSString stringWithFormat:@"Distance: %@ km", [formatter stringFromNumber:dist]];

            NSNumber *ti = [[objects firstObject] valueForKey:@"duration"];
            NSInteger seconds = ti.intValue % 60;
            NSInteger minutes = (ti.intValue / 60) %60;
            NSInteger hours = (ti.intValue / 3600);
            _trackDuration.text = [NSString stringWithFormat:@"Duration: %02ldh:%02ldm:%02lds",(long)hours,(long)minutes,(long)seconds];
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setDateFormat:@"dd.MM.yyyy - hh:mm"];
            NSString *dateString = [format stringFromDate:[[objects firstObject] valueForKey:@"date"]];
            _trackDate.text = dateString;
            
            _trackDetails.text = [[objects firstObject] valueForKey:@"descriere"];
            [_trackDetails sizeToFit];
            
            PFQuery *trackPointsQuery = [PFQuery queryWithClassName:@"Trackpoints"];
            [trackPointsQuery orderByAscending:@"counter"];
            [trackPointsQuery whereKey:@"track_id" equalTo:_trackId];
            [trackPointsQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if(!error){
                    for(PFObject *trackPoint in objects){
                        PFGeoPoint *trackPointCoordinates = [trackPoint valueForKey:@"coordinates"];
                        [_mPath addLatitude:[trackPointCoordinates latitude] longitude:[trackPointCoordinates longitude]];
                    }
                    PFGeoPoint *firstPointCoordinates = [[objects firstObject] valueForKey:@"coordinates"];
                    [_myMapView animateToLocation:CLLocationCoordinate2DMake([firstPointCoordinates latitude], [firstPointCoordinates longitude])];
                    _myMapView.camera = [GMSCameraPosition cameraWithLatitude: [firstPointCoordinates latitude] longitude:[firstPointCoordinates longitude] zoom:17];
                    [_mPolyline setPath:_mPath];
                    _mPolyline.map  = _myMapView;
                    [self.view addSubview:_myMapView];
                }
            }];
        }
    }];

    
}

- (void) initInfoView {
    _infoView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height/2-40, self.view.frame.size.width, self.view.frame.size.height/2)];
    _infoView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    
    _userPicture = [[FBProfilePictureView alloc]initWithFrame:CGRectMake(20, 20, 40, 40)];
    _userPicture.layer.cornerRadius = 5;
    [_infoView addSubview:_userPicture];
    
    _trackTitle = [[UILabel alloc] initWithFrame:CGRectMake(80, 20, 295, 20)];
    _trackTitle.text = @"Sample title";
    [_infoView addSubview:_trackTitle];
    
    _trackUserName = [[UILabel alloc]initWithFrame:CGRectMake(80, 40, 150, 20)];
    _trackUserName.text = @"by Sample user name";
    _trackUserName.textColor = [UIColor grayColor];
    _trackUserName.adjustsFontSizeToFitWidth = NO;
    _trackUserName.font = [UIFont systemFontOfSize:12];
    [_infoView addSubview:_trackUserName];
    
    _trackDate = [[UILabel alloc]initWithFrame:CGRectMake(230, 40, 120, 20)];
    _trackDate.text = @"sample date";
    _trackDate.textColor = [UIColor grayColor];
    _trackDate.adjustsFontSizeToFitWidth = NO;
    _trackDate.font = [UIFont systemFontOfSize:12];
    [_infoView addSubview:_trackDate];
    
    _trackDistance = [[UILabel alloc] initWithFrame:CGRectMake(20, 80, 335, 20)];
    _trackDistance.text = @"Distance: ";
    _trackDistance.adjustsFontSizeToFitWidth = NO;
    _trackDistance.font = [UIFont systemFontOfSize:12];
    [_infoView addSubview:_trackDistance];
    
    _trackDuration = [[UILabel alloc] initWithFrame:CGRectMake(20, 100, 335, 20)];
    _trackDuration.text = @"Duration: ";
    _trackDuration.adjustsFontSizeToFitWidth = NO;
    _trackDuration.font = [UIFont systemFontOfSize:12];
    [_infoView addSubview:_trackDuration];
    
    _trackDetails = [[UILabel alloc] initWithFrame:CGRectMake(20, 140, 335, 100)];
    _trackDetails.text = @"";
    _trackDetails.adjustsFontSizeToFitWidth = NO;
    _trackDetails.font = [UIFont systemFontOfSize:14];
    _trackDetails.numberOfLines = 0;
    //[_trackDetails sizeToFit];
    [_infoView addSubview:_trackDetails];
    
    _infoView.contentSize = CGSizeMake(350, 500);
    
    [self.view addSubview:_infoView];
}

- (void) getUserName: (NSString *) user_id {
    
    NSArray *permissions = [[NSArray alloc] initWithObjects:@"user_friends" ,@"email",@"basic_info", nil];
    
    [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {}];
    
    [FBRequestConnection startWithGraphPath:user_id completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        _trackUserName.text =  [result objectForKey:@"name"];
        NSLog(_userName);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
