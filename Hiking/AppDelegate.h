//
//  AppDelegate.h
//  Hiking
//
//  Created by LaboratoriOS Cronian Academy on 05/03/15.
//  Copyright (c) 2015 Hiking Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapViewController.h"
#import "NewsFeedViewController.h"
#import "ProfileViewController.h"
#import <GoogleMaps/GoogleMaps.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UITabBarController *mainTabBarController;
@property (strong, nonatomic) MapViewController *mapViewController;
@property (strong, nonatomic) NewsFeedViewController * newsFeedViewController;
@property (strong, nonatomic) ProfileViewController * profileViewController;
@end

