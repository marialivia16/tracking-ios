//
//  ProfileViewController.h
//  Hiking
//
//  Created by Maria Livia on 3/12/15.
//  Copyright (c) 2015 Hiking Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "HistoryViewController.h"
#import "FavoriteViewController.h"
#import <Parse/Parse.h>
#import <Bolts/Bolts.h>

@interface ProfileViewController : UIViewController
@property (strong, nonatomic) HistoryViewController * historyViewController;
@property (strong, nonatomic) FavoriteViewController * favoriteViewController;
@end
