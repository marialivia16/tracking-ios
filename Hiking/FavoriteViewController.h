//
//  FavoriteViewController.h
//  Hiking
//
//  Created by Maria Livia on 4/2/15.
//  Copyright (c) 2015 Hiking Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "CustomCell.h"
#import "TrackDetailViewController.h"

@interface FavoriteViewController : UIViewController

@end
