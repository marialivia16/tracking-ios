//
//  HistoryViewController.h
//  Hiking
//
//  Created by Maria Livia on 3/31/15.
//  Copyright (c) 2015 Hiking Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "CustomCell.h"
#import "TrackDetailViewController.h"

@interface HistoryViewController : UIViewController

@end
