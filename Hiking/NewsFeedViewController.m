//
//  NewsFeedViewController.m
//  Hiking
//
//  Created by LaboratoriOS Cronian Academy on 10/03/15.
//  Copyright (c) 2015 Hiking Inc. All rights reserved.
//

#import "NewsFeedViewController.h"
#import "UIImage+Helpers.h"
#import <FacebookSDK/FacebookSDK.h>
#import "Reachability.h"

@interface NewsFeedViewController () <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) UITableView *newsTableView;
@property (strong, nonatomic) UIRefreshControl *myRefreshControl;
@property (strong, nonatomic) NSMutableArray *isFav;
@property (strong, nonatomic) NSMutableArray *friendsTracks;
@property (strong, nonatomic) NSString *userId;
@property NSIndexPath *currentPath;
@property int cellCount;
@property Reachability *reachability;
@end

@implementation NewsFeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    _friendsTracks = [[NSMutableArray alloc] init];
    _isFav = [[NSMutableArray alloc] init];
    _cellCount = 0;
    
    if(remoteHostStatus == NotReachable){
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.view.bounds.size.width, self.view.bounds.size.height)];
        messageLabel.text = @"No internet connection.\nVerify your connection and pull down to refresh.";
        messageLabel.textColor = [UIColor grayColor];
        messageLabel.numberOfLines = 2;
        messageLabel.font = [UIFont systemFontOfSize:12];
        messageLabel.textAlignment = NSTextAlignmentCenter;
        //[messageLabel sizeToFit];
        [self.view addSubview:messageLabel];
    }
    else{
        [self addNewsTableView];
        [self getFacebookFriends];
    }
}

-(void) getFacebookFriends{
    NSArray *permissions = [[NSArray alloc] initWithObjects:@"user_friends" ,@"email",@"basic_info", nil];

    [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
        
    }];
    
    [FBRequestConnection startWithGraphPath:@"me" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        _userId = [result objectForKey:@"id"];
        //NSLog(@"USER ID FETCH %@", _userId);
    }];
    [FBRequestConnection startWithGraphPath:@"me/friends" completionHandler:^(FBRequestConnection *connection, NSDictionary *result, NSError *error){
        if(error == nil){
            NSArray *friends = [result objectForKey:@"data"];
            NSLog(@"Found %lu friends", (unsigned long)friends.count);
            NSMutableArray *ids = [[NSMutableArray alloc] init];
            for(NSDictionary<FBGraphUser> *friend in friends){
                NSLog(@"I have a friend named %@ with id %@", friend.name, friend.objectID);
                [ids addObject:friend.objectID];
            }
            PFQuery *query = [PFQuery queryWithClassName:@"Track"];
            query.limit = 5;
            [query orderByDescending:@"date"];
            query.skip = _cellCount;
            [query whereKey:@"user_id" containedIn:ids];
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if(!error){
                    NSLog(@"Successfully retrieved %lu tracks.", (unsigned long)objects.count);
                    _cellCount += objects.count;
                    NSLog(@"%d numar total de celule",_cellCount);
                    [_friendsTracks addObjectsFromArray:objects];
                    for (PFObject *object in objects) {
                        //NSLog(@"Track Id %@", [object valueForKey:@"track_id"]);
                        PFQuery *queryFavs = [PFQuery queryWithClassName:@"User_favs"];
                        [queryFavs whereKey:@"track_id" equalTo:[object valueForKey:@"track_id"]];
                        [queryFavs whereKey:@"user_id" equalTo:_userId];
                         NSArray *result = [queryFavs findObjects];
                         if(result.count != 0){
                             [_isFav addObject:@YES];
                             //NSLog(@"TRACK %@ is in favs", [object valueForKey:@"track_id"]);
                          } else{
                            [_isFav addObject:@NO];
                              //NSLog(@"TRACK %@ is NOT in favs", [object valueForKey:@"track_id"]);
                          }
                    }
                    //[self addNewsTableView];
                    [_newsTableView reloadData];
                }
            }];
        }
        else{
            NSLog(@"Error when retrieveing friends");
        }
    }];
    
}

- (void) refreshTable {
    _friendsTracks = [[NSMutableArray alloc] init];
    _isFav = [[NSMutableArray alloc] init];
    _cellCount = 0;
    [self getFacebookFriends];
    [_myRefreshControl endRefreshing];
}

-(void) addNewsTableView{
    _newsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 120)];
    _newsTableView.allowsSelection = NO;
    [_newsTableView setDelegate:self];
    [_newsTableView setDataSource:self];
    [self.view addSubview:_newsTableView];
    
    _myRefreshControl = [[UIRefreshControl alloc]init];
    [_myRefreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    [_newsTableView addSubview:_myRefreshControl];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _cellCount;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"CustomCell";
    _currentPath = indexPath;
    CustomCell *cell = (CustomCell *)[_newsTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CustomCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        //cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.titleLabel.text = (NSString *)[[_friendsTracks objectAtIndex:indexPath.row] valueForKey:@"name"];
    cell.userPicture.profileID = [[_friendsTracks objectAtIndex:indexPath.row] valueForKey:@"user_id"];
    cell.userPicture.layer.cornerRadius = 5;
    if([[[_friendsTracks objectAtIndex:indexPath.row] valueForKey:@"descriere"] isEqual:NSNull.null]){
        NSLog(@"abc");
        cell.descriptionLabel.text = @"No description";
    }
    else{
        cell.descriptionLabel.text = [[_friendsTracks objectAtIndex:indexPath.row] valueForKey:@"descriere"];
    }
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    NSNumber *dist = [[_friendsTracks objectAtIndex:indexPath.row] valueForKey:@"distance"];
    cell.kmLabel.text = [NSString stringWithFormat:@"%@ km", [formatter stringFromNumber:dist]];
    
    NSNumber *ti = [[_friendsTracks objectAtIndex:indexPath.row] valueForKey:@"duration"];
    NSInteger seconds = ti.intValue % 60;
    NSInteger minutes = (ti.intValue / 60) %60;
    NSInteger hours = (ti.intValue / 3600);
    cell.timeLabel.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",(long)hours,(long)minutes,(long)seconds];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd.MM.yyyy - hh:mm"];
    NSString *dateString = [format stringFromDate:[[_friendsTracks objectAtIndex:indexPath.row] valueForKey:@"date"]];
    cell.dateLabel.text = dateString;
    
    [cell.detailsButton addTarget:self action:@selector(navigateToTrackDetails:) forControlEvents:UIControlEventTouchUpInside];
    cell.detailsButton.tag = [[[_friendsTracks objectAtIndex:indexPath.row] valueForKey:@"track_id"] intValue];
    
    [cell.favIcon addTarget:self action:@selector(addToFavs:) forControlEvents:UIControlEventTouchUpInside];
    //[cell.favIcon setBackgroundImage:[UIImage imageNamed:@"FavouriteIconSel"] forState:UIControlStateHighlighted];
    cell.favIcon.tag = [[[_friendsTracks objectAtIndex:indexPath.row] valueForKey:@"track_id"] intValue];
    
    if([[_isFav objectAtIndex:indexPath.row] isEqual:@YES]){
        [cell.favIcon setImage:[[UIImage imageNamed:@"FavoriteIconSel"] imageWithColor:[UIColor colorWithRed:50/255.0f green:200/255.0f blue:180/255.0f alpha:1.0f]] forState:UIControlStateNormal];
    }
    else{
        [cell.favIcon setImage:[UIImage imageNamed:@"FavoriteIcon"] forState:UIControlStateNormal];
    }
    
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    

    if(_friendsTracks.count != 0){
        _newsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 1;
    }
    else{
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        messageLabel.text = @"No data is currently available. Please pull down to refresh or wait until data is received.";
        messageLabel.textColor = [UIColor grayColor];
        messageLabel.numberOfLines = 2;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont systemFontOfSize:12];
        [messageLabel sizeToFit];
        _newsTableView.backgroundView = messageLabel;
        _newsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return 0;
}

-(void)addToFavs:(id)sender{
    UIButton *btn = (UIButton *)sender;
    NSInteger trackID = btn.tag;
    NSLog(@"Button Clicked %li", (long)trackID);
    CGPoint hitPoint = [sender convertPoint:CGPointZero toView:_newsTableView];
    NSIndexPath *hitIndex = [_newsTableView indexPathForRowAtPoint:hitPoint];
    if([btn.currentImage isEqual:[UIImage imageNamed:@"FavoriteIcon"]]){
        PFObject *fav = [PFObject objectWithClassName:@"User_favs"];
        fav[@"track_id"] = [NSString stringWithFormat:@"%ld",(long)trackID];
        fav[@"user_id"] = _userId;
        [fav saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if(succeeded){
                [btn setImage:[[UIImage imageNamed:@"FavoriteIconSel"] imageWithColor:[UIColor colorWithRed:50/255.0f green:200/255.0f blue:180/255.0f alpha:1.0f]] forState:UIControlStateNormal];
                [_isFav setObject:@YES atIndexedSubscript:hitIndex.row];
            }
            else{
                
            }
        }];
    }
    else{
        NSLog(@"Already in favs");
        PFQuery *query = [PFQuery queryWithClassName:@"User_favs"];
        [query whereKey:@"track_id" equalTo:[NSString stringWithFormat:@"%ld",(long)trackID]];
        [query whereKey:@"user_id" equalTo:_userId];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if(!error){
                for(PFObject *object in objects){
                    [object deleteInBackground];
                }
                [btn setImage:[UIImage imageNamed:@"FavoriteIcon"] forState:UIControlStateNormal];
                [_isFav setObject:@NO atIndexedSubscript:hitIndex.row];
            }
            else{
                
            }
        }];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}

- (void) navigateToTrackDetails:(id)sender{
    UIButton *btn = (UIButton *)sender;
    NSString *trackId = [NSString stringWithFormat:@"%ld",(long)btn.tag];
    TrackDetailViewController *trackDetailViewController = [[TrackDetailViewController alloc] initWithTrack:trackId];
    trackDetailViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:trackDetailViewController animated:YES];
}

//- (void) scrollViewDidScroll:(UIScrollView *)scrollView{
//    CGPoint offset = scrollView.contentOffset;
//    CGRect bounds = scrollView.bounds;
//    CGSize size = scrollView.contentSize;
//    UIEdgeInsets inset = scrollView.contentInset;
//    float y = offset.y + bounds.size.height - inset.bottom;
//    float h = size.height;
//    
//    float reload_distance = 100;
//    if(y > h+reload_distance){
//        NSLog(@"Please load more cell");
//        [self getFacebookFriends];
//    }
//}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 100;
    if(y > h+reload_distance){
        NSLog(@"Please load more cell");
        [self getFacebookFriends];
        //[_newsTableView scrollToRowAtIndexPath:_cellCount - 5 atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
