//
//  ViewController.m
//  Hiking
//
//  Created by LaboratoriOS Cronian Academy on 05/03/15.
//  Copyright (c) 2015 Hiking Inc. All rights reserved.
//

#import "MapViewController.h"
#import "UIImage+Helpers.h"

@interface MapViewController () 

@end

@implementation MapViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"Map";
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    _mLocationArray = [[NSMutableArray alloc]init];
    
    //_myCameraPosition = [GMSCameraPosition cameraWithLatitude:37.36  longitude:-122.0 zoom:16];
    _myMapView = [GMSMapView mapWithFrame:self.view.bounds camera:_myCameraPosition];
    _myMapView.mapType = kGMSTypeHybrid;
    [self.view addSubview:_myMapView];
    
    
    if(nil == _mLocationManager)
        _mLocationManager = [[CLLocationManager alloc]init];
    
    _mLocationManager.delegate = self;
    _mLocationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    
    _mLocationManager.distanceFilter = 10;
    [_mLocationManager requestAlwaysAuthorization];
    
    
    _mPath = [GMSMutablePath path];
    
    _mPolyline = [GMSPolyline polylineWithPath:_mPath];
    _mPolyline.map = _myMapView;
    _mPolyline.strokeColor = [UIColor blueColor];
    _mPolyline.strokeWidth = 3.f;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNetworkChange:) name:kReachabilityChangedNotification object:nil];
    
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable) {NSLog(@"no");}
    else if (remoteHostStatus == ReachableViaWiFi) {NSLog(@"wifi"); }
    else if (remoteHostStatus == ReachableViaWWAN) {NSLog(@"cell"); }
    
    [self initInfoView];
    
}

- (void) handleNetworkChange:(NSNotification *)notice
{
    
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable) {NSLog(@"no");}
    else if (remoteHostStatus == ReachableViaWiFi) {NSLog(@"wifi"); }
    else if (remoteHostStatus == ReachableViaWWAN) {NSLog(@"cell"); }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) createParseTrack {
    
    _counter = [NSNumber numberWithInt:0];
    _startDate = [NSDate date];
    _track = [PFObject objectWithClassName:@"Track"];
    _track[@"date"] = _startDate;
    _mTrackId = [NSString stringWithFormat:@"%i",arc4random()];
    _track[@"track_id"] = _mTrackId;
    
    _trackDistance = [NSNumber numberWithDouble:0];
    
}

- (void) createParseTrackPointwithLocation: (CLLocation *) location {
    
    PFObject *currentTrackPoint = [PFObject objectWithClassName:@"Trackpoints"];
    PFGeoPoint *point = [PFGeoPoint geoPointWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude];
    currentTrackPoint[@"coordinates"] = point;
    currentTrackPoint[@"counter"] = _counter;
    currentTrackPoint[@"altitude"] = [NSNumber numberWithDouble:location.altitude];
    currentTrackPoint[@"track_id"] = _mTrackId;
    
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if(([FBSession activeSession].state == FBSessionStateOpen || [FBSession activeSession].state == FBSessionStateCreatedTokenLoaded)&& (remoteHostStatus == ReachableViaWiFi || remoteHostStatus == ReachableViaWWAN)){
        [currentTrackPoint saveInBackground];
        NSLog(@"save trackpoint");
    }
    else{
        [currentTrackPoint pinInBackground];
        NSLog(@"pin trackpoint");
    }
    
}

- (void) startTracking {
    [_startTrackingBtn removeTarget:self action:@selector(startTracking) forControlEvents:UIControlEventTouchUpInside];
    [_startTrackingBtn addTarget:self action:@selector(stopTracking) forControlEvents:UIControlEventTouchUpInside];
    [_startTrackingBtn setImage:[[[UIImage imageNamed:@"StopButton"] imageWithColor:[UIColor colorWithRed:0.80 green:0.80 blue:0.80 alpha:1.0]] scaledToSize:CGSizeMake(60, 60)] forState:UIControlStateNormal];
    [_mLocationManager startUpdatingLocation];
    [_mPath removeAllCoordinates];
    [self createParseTrack];
}

- (void) stopTracking {
    [_mLocationManager stopUpdatingLocation];
    [_startTrackingBtn removeTarget:self action:@selector(stopTracking) forControlEvents:UIControlEventTouchUpInside];
    [_startTrackingBtn addTarget:self action:@selector(startTracking) forControlEvents:UIControlEventTouchUpInside];
    [_startTrackingBtn setImage:[[[UIImage imageNamed:@"StartButton"] imageWithColor:[UIColor colorWithRed:0.80 green:0.80 blue:0.80 alpha:1.0]] scaledToSize:CGSizeMake(60, 60)] forState:UIControlStateNormal];
    
    _stopDate = [NSDate date];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Track details" message:@"Track title:" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    [alert textFieldAtIndex:1].secureTextEntry = NO;
    [alert textFieldAtIndex:1].placeholder = @"Enter a description for your track";
    [alert textFieldAtIndex:1].keyboardType = UIKeyboardTypeDefault;
    [alert textFieldAtIndex:0].placeholder = @"Enter a title for your track";
    [alert textFieldAtIndex:0].keyboardType = UIKeyboardTypeDefault;
    [alert show];
    [_mPath removeAllCoordinates];
    [_mPolyline setPath:_mPath];
    _mPolyline.map  = _myMapView;
    [self.view addSubview:_myMapView];
    [self initInfoView];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    NSNumber *speed;
    
    CLLocation *location = [locations lastObject];
    
    int n = [_counter intValue];
    
    
    if([_counter intValue] > 0){
        [_mPath addLatitude:location.coordinate.latitude longitude:location.coordinate.longitude];
        [_myMapView animateToLocation:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)];
        [_myMapView setCamera:[GMSCameraPosition cameraWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude zoom:16]];
    
        if([_mPath count] > 2){
            [_mPolyline setPath:_mPath];
            _mPolyline.map  = _myMapView;
            [self.view addSubview:_myMapView];
        }
    
        speed = [NSNumber numberWithDouble:(location.speed * 3.6)];
    
        if([_counter intValue] == 1){
            _lastLocation = location;
        }
    
        _trackDistance  = [NSNumber numberWithDouble:[location distanceFromLocation:_lastLocation] + _trackDistance.doubleValue];
        _lastLocation = location;
        NSLog(@"Distanta parcursa @%f",_trackDistance.doubleValue);
    
        [self createParseTrackPointwithLocation:location];
        
    }
    _counter = [NSNumber numberWithInt:n + 1];
    [self addLabelWithSpeed:speed ];
    [self addLabelWithTime:[NSDate date]];
    [self addLabelWithDistance:_trackDistance];
    
    //[self.view addSubview:_startTrackingBtn];
    [self.view addSubview:_infoView];
}

- (void) addLabelWithSpeed:(NSNumber *)speed{
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    _speedLabel.text = [NSString stringWithFormat:@"%@ km/h", [formatter stringFromNumber:speed]];
    [_infoView addSubview:_speedLabel];
}

- (void) addLabelWithDistance:(NSNumber *)distance{
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    _distanceLabel.text = [NSString stringWithFormat:@"%@ km", [formatter stringFromNumber:[NSNumber numberWithDouble:[distance doubleValue] / 1000]]];
    [_infoView addSubview:_distanceLabel];
}

- (void) addLabelWithTime:(NSDate *)date{
    
    NSInteger ti = (NSInteger) [date timeIntervalSinceDate:_startDate];
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) %60;
    NSInteger hours = (ti / 3600);
    
    _timeLabel.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",(long)hours,(long)minutes,(long)seconds];
    
    [_infoView addSubview:_timeLabel];
    
}

- (void) alertView:(UIAlertView *) alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //NSLog(@"Entered: %@",[[alertView textFieldAtIndex:0] text]);
    _trackName = [[alertView textFieldAtIndex:0] text];
    _trackDescription = [[alertView textFieldAtIndex:1]text];
    [self updateParseTrack];
}

- (void) updateParseTrack {
    _track[@"name"] = _trackName;
    _track[@"descriere"] = _trackDescription;
    _track[@"distance"] = [NSNumber numberWithDouble:_trackDistance.doubleValue / 1000];
    _track[@"duration"] = [NSNumber numberWithInteger: [_stopDate timeIntervalSinceDate:_startDate]]; //duration in minutes
    
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if(([FBSession activeSession].state == FBSessionStateOpen || [FBSession activeSession].state == FBSessionStateCreatedTokenLoaded) && (remoteHostStatus == ReachableViaWiFi || remoteHostStatus == ReachableViaWWAN)){
        
        NSArray *permissions = [[NSArray alloc] initWithObjects:@"user_friends" ,@"email",@"basic_info", nil];
        
        [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
            
        }];
        
        [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *aUser, NSError *error) {
            if(!error){
                _track[@"user_id"] = aUser.objectID;
                [_track saveInBackground];
                NSLog(@"Salveaza");
                NSLog(aUser.objectID);
            }
            
            else{
                [_track pinInBackground];
                NSLog(@"pin");
            }
        }];
        
    }
    else{
        [_track pinInBackground];
        NSLog(@"pin without facebook");
    }
}

- (void) initInfoView {
    _infoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    _infoView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    
    _startTrackingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_startTrackingBtn setImage:[[[UIImage imageNamed:@"StartButton"] imageWithColor:[UIColor colorWithRed:0.80 green:0.80 blue:0.80 alpha:1.0]] scaledToSize:CGSizeMake(60, 60)] forState:UIControlStateNormal];
    _startTrackingBtn.frame = CGRectMake(_infoView.frame.size.width / 2 - 40, _infoView.frame.size.height / 2 - 40, 80, 80);
    [_startTrackingBtn addTarget:self action:@selector(startTracking) forControlEvents:UIControlEventTouchUpInside];
    [_infoView addSubview:_startTrackingBtn];
    
    _speedLabel = [[UILabel alloc]initWithFrame:CGRectMake(_infoView.frame.size.width-100, _infoView.frame.size.height-50, 100, 50)];
    _speedLabel.textColor = [UIColor whiteColor];
    _speedLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    _speedLabel.textAlignment = NSTextAlignmentCenter;
    _speedLabel.numberOfLines = 0;
    
    _distanceLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, 100, 50)];
    _distanceLabel.textColor = [UIColor whiteColor];
    _distanceLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    _distanceLabel.textAlignment = NSTextAlignmentCenter;
    _distanceLabel.numberOfLines = 0;
    
    _timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 50)];
    _timeLabel.textColor = [UIColor whiteColor];
    _timeLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    _timeLabel.textAlignment = NSTextAlignmentCenter;
    _timeLabel.numberOfLines = 0;
    
    [_infoView addSubview:_distanceLabel];
    [_infoView addSubview:_speedLabel];
    [_infoView addSubview:_timeLabel];

    
    [self.view addSubview:_infoView];
}

@end
