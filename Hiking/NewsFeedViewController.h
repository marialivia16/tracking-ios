//
//  NewsFeedViewController.h
//  Hiking
//
//  Created by LaboratoriOS Cronian Academy on 10/03/15.
//  Copyright (c) 2015 Hiking Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "CustomCell.h"
#import "TrackDetailViewController.h"

@interface NewsFeedViewController : UIViewController
@property (strong, nonatomic) TrackDetailViewController *trackViewController;
@end
