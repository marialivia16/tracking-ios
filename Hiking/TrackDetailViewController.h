//
//  TrackDetailViewController.h
//  Hiking
//
//  Created by Maria Livia on 4/28/15.
//  Copyright (c) 2015 Hiking Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <Parse/Parse.h>
#import <FacebookSDK/FacebookSDK.h>

@interface TrackDetailViewController : UIViewController
@property (strong, nonatomic) NSString *trackId;
@property (strong, nonatomic)  GMSMapView *myMapView;
@property (strong, nonatomic) GMSCameraPosition *myCameraPosition;
@property (nonatomic) GMSMutablePath *mPath;
@property (nonatomic) GMSPolyline *mPolyline;
@property (nonatomic) UIScrollView * infoView;
@property (nonatomic, strong) FBProfilePictureView *userPicture;
@property (strong, nonatomic) NSString *userName;
@property UILabel *trackTitle, *trackUserName, *trackDistance, *trackDuration, *trackDate, *trackDetails;
- (instancetype) initWithTrack:(NSString *)trackId;
@end
