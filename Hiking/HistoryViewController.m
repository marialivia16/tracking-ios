//
//  HistoryViewController.m
//  Hiking
//
//  Created by Maria Livia on 3/31/15.
//  Copyright (c) 2015 Hiking Inc. All rights reserved.
//

#import "HistoryViewController.h"
#import "UIImage+Helpers.h"
#import <FacebookSDK/FacebookSDK.h>
#import "Reachability.h"

@interface HistoryViewController () <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) UITableView *historyTableView;
@property (strong, nonatomic) UIRefreshControl *myRefreshControl;
@property (strong, nonatomic) NSMutableArray *friendsTracks;
@property (strong, nonatomic) NSString *userId;
@property NSIndexPath *currentPath;
@property int cellCount;
@property Reachability *reachability;
@end

@implementation HistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    _friendsTracks = [[NSMutableArray alloc] init];
    _cellCount = 0;
    
    if(remoteHostStatus == NotReachable){
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.view.bounds.size.width, self.view.bounds.size.height)];
        messageLabel.text = @"No internet connection.\nVerify your connection and pull down to refresh.";
        messageLabel.textColor = [UIColor grayColor];
        messageLabel.numberOfLines = 2;
        messageLabel.font = [UIFont systemFontOfSize:12];
        messageLabel.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:messageLabel];
    }
    else{
        [self addNewsTableView];
        [self getFacebookFriends];
    }
    self.title = @"History";
}

-(void) getFacebookFriends{
    NSArray *permissions = [[NSArray alloc] initWithObjects:@"user_friends" ,@"email",@"basic_info", nil];
    
    [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
        
    }];
    
    [FBRequestConnection startWithGraphPath:@"me" completionHandler:^(FBRequestConnection *connection, id result, NSError *error){
        if(error == nil){
            _userId = [result objectForKey:@"id"];
            PFQuery *query = [PFQuery queryWithClassName:@"Track"];
            query.limit = 5;
            [query orderByDescending:@"date"];
            query.skip = _cellCount;
            [query whereKey:@"user_id" equalTo:_userId];
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if(!error){
                    NSLog(@"Successfully retrieved %lu tracks.", (unsigned long)objects.count);
                    _cellCount += objects.count;
                    NSLog(@"%d numar total de celule",_cellCount);
                    [_friendsTracks addObjectsFromArray:objects];
                    [_historyTableView reloadData];
                }
            }];
        }
        else{
            NSLog(@"Error when retrieveing friends");
        }
    }];
    
}

- (void) refreshTable {
    _friendsTracks = [[NSMutableArray alloc] init];
    _cellCount = 0;
    [self getFacebookFriends];
    [_myRefreshControl endRefreshing];
}

-(void) addNewsTableView{
    _historyTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 70)];
    _historyTableView.allowsSelection = NO;
    [_historyTableView setDelegate:self];
    [_historyTableView setDataSource:self];
    [self.view addSubview:_historyTableView];
    
    _myRefreshControl = [[UIRefreshControl alloc]init];
    [_myRefreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    [_historyTableView addSubview:_myRefreshControl];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _cellCount;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"CustomCell";
    _currentPath = indexPath;
    CustomCell *cell = (CustomCell *)[_historyTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CustomCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.titleLabel.text = (NSString *)[[_friendsTracks objectAtIndex:indexPath.row] valueForKey:@"name"];
    cell.userPicture.profileID = [[_friendsTracks objectAtIndex:indexPath.row] valueForKey:@"user_id"];
    cell.userPicture.layer.cornerRadius = 5;
    if([[[_friendsTracks objectAtIndex:indexPath.row] valueForKey:@"descriere"] isEqual:NSNull.null]){
        NSLog(@"abc");
        cell.descriptionLabel.text = @"No description";
    }
    else{
        cell.descriptionLabel.text = [[_friendsTracks objectAtIndex:indexPath.row] valueForKey:@"descriere"];
    }
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    NSNumber *dist = [[_friendsTracks objectAtIndex:indexPath.row] valueForKey:@"distance"];
    cell.kmLabel.text = [NSString stringWithFormat:@"%@ km", [formatter stringFromNumber:dist]];
    
    NSNumber *ti = [[_friendsTracks objectAtIndex:indexPath.row] valueForKey:@"duration"];
    NSInteger seconds = ti.intValue % 60;
    NSInteger minutes = (ti.intValue / 60) %60;
    NSInteger hours = (ti.intValue / 3600);
    cell.timeLabel.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",(long)hours,(long)minutes,(long)seconds];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd.MM.yyyy - hh:mm"];
    NSString *dateString = [format stringFromDate:[[_friendsTracks objectAtIndex:indexPath.row] valueForKey:@"date"]];
    cell.dateLabel.text = dateString;
    
    [cell.detailsButton addTarget:self action:@selector(navigateToTrackDetails:) forControlEvents:UIControlEventTouchUpInside];
    cell.detailsButton.tag = [[[_friendsTracks objectAtIndex:indexPath.row] valueForKey:@"track_id"] intValue];
    [cell.favIcon setImage:nil forState:UIControlStateNormal];
    
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    if(_friendsTracks.count != 0){
        _historyTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 1;
    }
    else{
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        messageLabel.text = @"No data is currently available. Please pull down to refresh or wait until data is received.";
        messageLabel.textColor = [UIColor grayColor];
        messageLabel.numberOfLines = 2;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont systemFontOfSize:12];
        [messageLabel sizeToFit];
        _historyTableView.backgroundView = messageLabel;
        _historyTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}

- (void) navigateToTrackDetails:(id)sender{
    UIButton *btn = (UIButton *)sender;
    NSString *trackId = [NSString stringWithFormat:@"%ld",(long)btn.tag];
    TrackDetailViewController *trackDetailViewController = [[TrackDetailViewController alloc] initWithTrack:trackId];
    trackDetailViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:trackDetailViewController animated:YES];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 100;
    if(y > h+reload_distance){
        NSLog(@"Please load more cell");
        [self getFacebookFriends];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
