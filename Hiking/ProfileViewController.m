//
//  ProfileViewController.m
//  Hiking
//
//  Created by Maria Livia on 3/12/15.
//  Copyright (c) 2015 Hiking Inc. All rights reserved.
//

#import "ProfileViewController.h"
#import <FacebookSDK/FacebookSDK.h>

@interface ProfileViewController () <FBLoginViewDelegate, UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) FBProfilePictureView *profilePictureView;
@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) UITableView *navTableView;
@property (strong, nonatomic) NSArray *navArray;
@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    FBLoginView *loginView = [[FBLoginView alloc] initWithReadPermissions:@[@"public_profile",@"email",@"user_friends"]];
    loginView.delegate = self;
    [loginView setFrame:CGRectMake(20, self.view.frame.size.height - 20 - 44 - 49 - 20 - loginView.frame.size.height, self.view.frame.size.width - 40, loginView.frame.size.height)];
    loginView.layer.cornerRadius = 10.0f;
    [self.view addSubview:loginView];
    
    _profilePictureView = [[FBProfilePictureView alloc] initWithFrame:CGRectMake(100, 50, self.view.frame.size.width - 200, self.view.frame.size.width - 200)];
    
    _profilePictureView.layer.cornerRadius = MIN( _profilePictureView.frame.size.width, _profilePictureView.frame.size.height) / 2;
    [self.view addSubview:_profilePictureView];
    
    _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_profilePictureView.frame) + 20, self.view.frame.size.width, 20)];
    _nameLabel.textAlignment = NSTextAlignmentCenter;
    _nameLabel.textColor = [UIColor grayColor];
    [self.view addSubview:_nameLabel];
    
    [self addNavigationTableView];
}

-(void) addNavigationTableView{
    _navArray = @[@"History", @"Favorites"];
    _navTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_nameLabel.frame) + 20, self.view.frame.size.width, 100)];
    _navTableView.bounces = NO;
    [_navTableView setDelegate:self];
    [_navTableView setDataSource:self];
    [self.view addSubview:_navTableView];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [_navTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.textLabel.text = [self.navArray objectAtIndex:indexPath.row];
    cell.textLabel.textColor = [UIColor grayColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([[self.navArray objectAtIndex:indexPath.row]  isEqual: @"History"]){
        _historyViewController = [[HistoryViewController alloc] init];
        _historyViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:_historyViewController animated:YES];
    }
    else if([[self.navArray objectAtIndex:indexPath.row]  isEqual: @"Favorites"]){
        _favoriteViewController = [[FavoriteViewController alloc] init];
        _favoriteViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:_favoriteViewController animated:YES];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user{
    self.profilePictureView.profileID = [user objectID];
    self.nameLabel.text = user.name;
    
    [self savePinnedParseObjectsWithUser:user];
    
    NSLog(@"just logged in");
}

- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    self.profilePictureView.profileID = nil;
    self.nameLabel.text = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//saves pinned objects, if any
- (void) savePinnedParseObjectsWithUser:(id<FBGraphUser>) user {
    
    PFQuery *queryTrack = [PFQuery queryWithClassName:@"Track"];
    [queryTrack fromLocalDatastore];
    [[queryTrack findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        if(task.error){
            NSLog(@"Error: %@", task.error);
            return task;
        }
        
        NSArray *tracks = task.result;
        
        for(PFObject *track in tracks){
            track[@"user_id"] = [user objectID];
            [[track saveInBackground] continueWithSuccessBlock:^id(BFTask *task) {
                return [track unpinInBackground];
            }];
        }
        
        NSLog(@"Retrieved %lu tracks", (unsigned long)[task.result count]);
        return task;
    }];
    
    PFQuery *queryTrackPoints = [PFQuery queryWithClassName:@"Trackpoints"];
    [queryTrackPoints fromLocalDatastore];
    [[queryTrackPoints findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        if(task.error){
            NSLog(@"Error: %@", task.error);
            return task;
        }
        
        NSArray *trackpoints = task.result;
        
        for(PFObject *trackpoint in trackpoints){
            [[trackpoint saveInBackground] continueWithSuccessBlock:^id(BFTask *task) {
                return [trackpoint unpinInBackground];
            }];
        }
        
        NSLog(@"Retrieved %lu points", (unsigned long)[task.result count]);
        return task;
    }];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
