### What is this repository for? ###

This is an iOS application developed in Objective-C. It allows users to log in using their Facebook account, track their walks or runs and then share them with their friends.